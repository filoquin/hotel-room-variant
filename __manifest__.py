{
    'name': 'Hotel room variant',
    'version': '10.0.1.0.0',
    'author': 'filoquin',
    'category': 'Hotel Management',
    'website': 'https://gitlab.com/filoquin',
    'depends': ['hotel_reservation'],
    'license': 'AGPL-3',
    'summary': 'Implement product variant in hotel room',
    'demo': [],
    'data': [
        'views/hotel_room.xml',
        'views/reservation.xml'
    ],
    'css': [],
    'images': [],
    'auto_install': False,
    'installable': True,
    'application': False
}