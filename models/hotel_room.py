from odoo import models, fields, api, _

import logging

_logger = logging.getLogger(__name__)



class product_template(models.Model):

    _inherit = "product.template"

    isroom = fields.Boolean('Is Room')
    iscategid = fields.Boolean('Is categ id')
    isservice = fields.Boolean('Is Service id')


class hotel_room(models.Model):

    _inherit = 'hotel.room'

    product_id = fields.Many2one('product.template', 'Product_id',
                                 required=True, delegate=True,
                                 ondelete='cascade')
