from odoo import models, fields, api, _
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as dt
from odoo.exceptions import except_orm, ValidationError
from odoo.fields import Datetime as odooDatetime 
import logging

_logger = logging.getLogger(__name__)


class quick_room_reservation(models.TransientModel):
    _inherit = 'quick.room.reservation'

    room_id = fields.Many2one('hotel.room', 'Room', required=True)
    product_id = fields.Many2one(
        'product.template',
        'Product_id',
        related = 'room_id.product_id'    
    )

    room_variant_id = fields.Many2one(
        'product.product',
        'Room Variant',
        required=True
    )
    warehouse_id = fields.Many2one(
        'stock.warehouse', 'Hotel', select=True, required=True)

    confirm_reserve = fields.Boolean(
        string='Confirm reserve',
        default = True,
    )
    create_folio = fields.Boolean(
        string='Create Folio',
        default = True,
    )

    create_invoice = fields.Boolean(
        string='Create invoice',
        default = False,
    )


    @api.model
    def default_get(self, fields):
        """
        To get default values for the object.
        @param self: The object pointer.
        @param fields: List of fields for which we want default values
        @return: A dictionary which of fields with values.
        """
        if self._context is None:
            self._context = {}
        res = super(quick_room_reservation, self).default_get(fields)
        if self._context:
            keys = self._context.keys()
            if 'date' in keys:
                _logger.info(self._context['date'])
                check_out = odooDatetime.to_string(
                    odooDatetime.from_string(self._context['date']) + timedelta(days=1)
                )
                res.update({'check_in': self._context['date'],'check_out':check_out})
            if 'room_id' in keys:
                roomid = self._context['room_id']
                res.update({'room_id': int(roomid)})
        return res


    @api.multi
    def room_reserve(self):
        """
        This method create a new record for hotel.reservation
        -----------------------------------------------------
        @param self: The object pointer
        @return: new record set for hotel reservation.
        """
        self.ensure_one()
        hotel_res_obj = self.env['hotel.reservation']
        for res in self:
            reservation = hotel_res_obj.create({
                      'partner_id': res.partner_id.id,
                     'partner_invoice_id': res.partner_invoice_id.id,
                     'partner_order_id': res.partner_order_id.id,
                     'partner_shipping_id': res.partner_shipping_id.id,
                     'checkin': res.check_in,
                     'checkout': res.check_out,
                     'warehouse_id': res.warehouse_id.id,
                     'pricelist_id': res.pricelist_id.id,
                     'adults': res.adults,
                     'reservation_line': [(0, 0,
                                           {
                                           #'reserve': res.room_id.id,
                                            'reserve': [(6, 0,[res.room_id.id])],
                                            'name': (res.room_variant_id and
                                                     res.room_variant_id.name or ''),
                                            'room_variant_id':res.room_variant_id.id
                                            })]
                     }
            )
            if self.confirm_reserve :
                reservation.confirmed_reservation()
                if self.create_folio:
                    reservation.create_folio()
                    reservation.folio_id.action_confirm()
            return reservation

        view_id = self.env.ref('hotel_reservation.view_hotel_reservation_form')
        view = { 
            'name':_("Reservation"),
            'view_mode': 'form',
            'view_id': view_id.id,
            'res_id': reservation.id,
            'view_type': 'form',
            'res_model': 'hotel.reservation',
            'type': 'ir.actions.act_window',
            'nodestroy': False,
            'target': 'new',
        }

        return view



class hotel_reservation_line(models.Model):
    _inherit = "hotel_reservation.line"

    room_variant_id = fields.Many2one(
        'product.product',
        'Variant',
        #required=True
    )

    """reserve = fields.Many2one(
        'hotel.room',
        'Room',
        domain="[('isroom','=',True),\
        ('categ_id','=',categ_id)]"
    )"""

    reserve = fields.Many2many('hotel.room',
                               'hotel_reservation_line_room_rel',
                               'hotel_reservation_line_id', 'room_id',
                               domain="[('isroom','=',True),\
                               ('categ_id','=',categ_id)]")

class hotel_reservation(models.Model):

    _inherit = "hotel.reservation"

    @api.multi
    def confirmed_reservation(self):
        """
        This method create a new recordset for hotel room reservation line
        ------------------------------------------------------------------
        @param self: The object pointer
        @return: new record set for hotel room reservation line.
        """
        reservation_line_obj = self.env['hotel.room.reservation.line']
        for reservation in self:
            for line_id in reservation.reservation_line:

                for room_id in line_id.reserve:

                    if room_id.room_reservation_line_ids:
                        for reserv in room_id.room_reservation_line_ids:
                            reserv_checkin = datetime.\
                                strptime(reservation.checkin, dt)
                            reserv_checkout = datetime.\
                                strptime(reservation.checkout, dt)
                            check_in = datetime.strptime(reserv.check_in, dt)
                            check_out = datetime.strptime(reserv.check_out, dt)
                            range1 = [reserv_checkin, reserv_checkout]
                            range2 = [check_in, check_out]
                            overlap_dates = self.check_overlap(*range1) \
                                & self.check_overlap(*range2)
                            if overlap_dates:
                                overlap_dates = [datetime.
                                                 strftime(dates,
                                                          '%d/%m/%Y') for
                                                 dates in overlap_dates]
                                raise ValidationError(_('You tried to Confirm '
                                                        'reservation with room'
                                                        ' those already '
                                                        'reserved in this '
                                                        'Reservation Period. '
                                                        'Overlap Dates are '
                                                        '%s') % overlap_dates)
                            else:
                                self.state = 'confirm'
                                for room_id in line_id.reserve:
                                    vals = {'room_id': room_id.id,
                                            'check_in': reservation.checkin,
                                            'check_out': reservation.checkout,
                                            'state': 'assigned',
                                            'reservation_id': reservation.id,
                                            }
                                    room_id.write({'isroom': False,
                                                   'status': 'occupied'})
                                    reservation_line_obj.create(vals)
                    else:
                        self.state = 'confirm'

                        for room_id in line_id.reserve:
                            vals = {'room_id': room_id.id,
                                    'check_in': reservation.checkin,
                                    'check_out': reservation.checkout,
                                    'state': 'assigned',
                                    'reservation_id': reservation.id,
                                    }
                            room_id.write({'isroom': False,
                                           'status': 'occupied'})

                            reservation_line_obj.create(vals)
#             self._cr.execute("select count(*) from hotel_reservation as hr "
#                              "inner join hotel_reservation_line as hrl on \
#                              hrl.line_id = hr.id "
#                              "inner join hotel_reservation_line_room_rel as \
#                              hrlrr on hrlrr.room_id = hrl.id "
#                              "where (checkin,checkout) overlaps \
#                              ( timestamp %s, timestamp %s ) "
#                              "and hr.id <> cast(%s as integer) "
#                              "and hr.state = 'confirm' "
#                              "and hrlrr.hotel_reservation_line_id in ("
#                              "select hrlrr.hotel_reservation_line_id \
#                              from hotel_reservation as hr "
#                              "inner join hotel_reservation_line as \
#                              hrl on hrl.line_id = hr.id "
#                              "inner join hotel_reservation_line_room_rel \
#                              as hrlrr on hrlrr.room_id = hrl.id "
#                              "where hr.id = cast(%s as integer) )",
#                              (reservation.checkin, reservation.checkout,
#                               str(reservation.id), str(reservation.id)))
#             res = self._cr.fetchone()
#             roomcount = res and res[0] or 0.0
#             if roomcount:
#                 raise ValidationError(_('You tried to confirm reservation \
#                 with room those already reserved in this reservation \
#                     period'))
#             else:
#                 self.state = 'confirm'
#                 for line_id in reservation.reservation_line:
#                     line_id = line_id.reserve
#                     for room_id in line_id:
#                         vals = {
#                             'room_id': room_id.id,
#                             'check_in': reservation.checkin,
#                             'check_out': reservation.checkout,
#                             'state': 'assigned',
#                             'reservation_id': reservation.id,
#                             }
#                         room_id.write({'isroom': False,
#                                          'status': 'occupied'})
#                         reservation_line_obj.create(vals)
        return True

    @api.multi
    def create_folio(self):
        """
        This method is for create new hotel folio.
        -----------------------------------------
        @param self: The object pointer
        @return: new record set for hotel folio.
        """
        hotel_folio_obj = self.env['hotel.folio']
        room_obj = self.env['hotel.room']
        for reservation in self:
            folio_lines = []
            checkin_date = reservation['checkin']
            checkout_date = reservation['checkout']
            if not self.checkin < self.checkout:
                raise except_orm(_('Error'),
                                 _('Checkout date should be greater \
                                 than the Checkin date.'))
            duration_vals = (self.onchange_check_dates
                             (checkin_date=checkin_date,
                              checkout_date=checkout_date, duration=False))
            duration = duration_vals.get('duration') or 0.0
            folio_vals = {
                'date_order': reservation.date_order,
                'warehouse_id': reservation.warehouse_id.id,
                'partner_id': reservation.partner_id.id,
                'pricelist_id': reservation.pricelist_id.id,
                'partner_invoice_id': reservation.partner_invoice_id.id,
                'partner_shipping_id': reservation.partner_shipping_id.id,
                'checkin_date': reservation.checkin,
                'checkout_date': reservation.checkout,
                'duration': duration,
                'reservation_id': reservation.id,
                'service_lines': reservation['folio_id']
            }
            for line in reservation.reservation_line:
                folio_lines.append((0, 0, {
                    'checkin_date': checkin_date,
                    'checkout_date': checkout_date,
                    #'product_id': r.product_id and r.product_id.id,
                    'product_id': line.room_variant_id and line.room_variant_id.id,
                    'name': reservation['reservation_no'],
                    'price_unit': line.room_variant_id.list_price,
                    'product_uom_qty': duration,
                    'is_reserved': True}))
                for r in line.reserve:
                    res_obj = room_obj.browse([r.id])
                    res_obj.write({'status': 'occupied', 'isroom': False})
            folio_vals.update({'room_lines': folio_lines})
            folio = hotel_folio_obj.create(folio_vals)
            if folio:
                for rm_line in folio.room_lines:
                    rm_line.product_id_change()
            self._cr.execute('insert into hotel_folio_reservation_rel'
                             '(order_id, invoice_id) values (%s,%s)',
                             (reservation.id, folio.id))
            self.state = 'done'
        return True
class hotel_room_reservation_line(models.Model):

    _inherit = 'hotel.room.reservation.line'
    room_variant_id = fields.Many2one(
        'product.product',
        'Variant',
        #required=True
    )

